package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Armud {
    public static void main(String[] args) {
        List<Integer> arr = new ArrayList<>();
        arr.add(15);
        arr.add(27);
        arr.add(14);
        arr.add(38);
        arr.add(26);
        arr.add(55);
        arr.add(46);
        arr.add(65);
        arr.add(85);
//        [15,27,14,38,26,55,46,65,85]
        System.out.println(longestIncreasingSubsequence(arr));

    }

    public static int longestIncreasingSubsequence(List<Integer> arr) {
        // Write your code here
        // Add boundary case, when array size is one
        int size = arr.size();
        int[] tailTable = new int[size];
        int len; // always points empty slot

        tailTable[0] = arr.get(0);
        len = 1;
        //        [15,27,14,38,26,55,46,65,85]
        for (int i = 1; i < size; i++) {
            if (arr.get(i) < tailTable[0])
                // new smallest value
                tailTable[0] = arr.get(i);

            else if (arr.get(i) > tailTable[len - 1])
                // A[i] wants to extend largest subsequence
                tailTable[len++] =arr.get(i);

            else
                // A[i] wants to be current end candidate of an existing
                // subsequence. It will replace ceil value in tailTable
                tailTable[CeilIndex(tailTable, -1, len - 1, arr.get(i))] = arr.get(i);
            Arrays.stream(tailTable).forEach(value ->{
                System.out.println(value);
            });
            System.out.println("------------------");
        }

        return len;

    }
    // Binary search (note boundaries in the caller)
    // A[] is ceilIndex in the caller
    static int CeilIndex(int A[], int l, int r, int key)
    {
        while (r - l > 1) {
            int m = l + (r - l) / 2;
            if (A[m] >= key)
                r = m;
            else
                l = m;
        }

        return r;
    }
}
